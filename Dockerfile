# Dockerfile
FROM node:11.13.0-alpine

WORKDIR /ps5-frontend

COPY ./package.json ./
COPY ./package-lock.json ./

RUN npm install

ENV NODE_ENV production

COPY . .

RUN npm run build

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

CMD [ "npm", "start" ]